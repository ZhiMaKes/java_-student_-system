package com.ruoyi.web.controller.teacher;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.teacher.domain.DzTeacherList;
import com.ruoyi.teacher.service.IDzTeacherListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师列表Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/teacher/teacherlist")
public class DzTeacherListController extends BaseController
{
    @Autowired
    private IDzTeacherListService dzTeacherListService;

    /**
     * 查询教师列表列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:list')")
    @GetMapping("/list")
    public TableDataInfo list(DzTeacherList dzTeacherList)


    {
//        System.out.println(dzTeacherList);
        startPage();
        List<DzTeacherList> list = dzTeacherListService.selectDzTeacherListList(dzTeacherList);

        return getDataTable(list);
    }

    /**
     * 导出教师列表列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:export')")
    @Log(title = "教师列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzTeacherList dzTeacherList)
    {
        List<DzTeacherList> list = dzTeacherListService.selectDzTeacherListList(dzTeacherList);
        ExcelUtil<DzTeacherList> util = new ExcelUtil<DzTeacherList>(DzTeacherList.class);
        util.exportExcel(response, list, "教师列表数据");
    }

    /**
     * 获取教师列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dzTeacherListService.selectDzTeacherListById(id));
    }

    /**
     * 新增教师列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:add')")
    @Log(title = "教师列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzTeacherList dzTeacherList)
    {
        return toAjax(dzTeacherListService.insertDzTeacherList(dzTeacherList));
    }

    /**
     * 修改教师列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:edit')")
    @Log(title = "教师列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzTeacherList dzTeacherList)

    {

        return toAjax(dzTeacherListService.updateDzTeacherList(dzTeacherList));
    }

    /**
     * 删除教师列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacherlist:remove')")
    @Log(title = "教师列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dzTeacherListService.deleteDzTeacherListByIds(ids));
    }
}
