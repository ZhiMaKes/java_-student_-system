package com.ruoyi.web.controller.platformusers;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.platformusers.domain.PlatformUsers;
import com.ruoyi.platformusers.service.IPlatformUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 平台用户信息Controller
 * 
 * @author ruoyi
 * @date 2024-01-05
 */
@RestController
@RequestMapping("/platformusers/platformusers")
public class PlatformUsersController extends BaseController
{
    @Autowired
    private IPlatformUsersService platformUsersService;

    /**
     * 查询平台用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:list')")
    @GetMapping("/list")
    public TableDataInfo list(PlatformUsers platformUsers)
    {
        startPage();
        List<PlatformUsers> list = platformUsersService.selectPlatformUsersList(platformUsers);
        return getDataTable(list);
    }

    /**
     * 导出平台用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:export')")
    @Log(title = "平台用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PlatformUsers platformUsers)
    {
        List<PlatformUsers> list = platformUsersService.selectPlatformUsersList(platformUsers);
        ExcelUtil<PlatformUsers> util = new ExcelUtil<PlatformUsers>(PlatformUsers.class);
        util.exportExcel(response, list, "平台用户信息数据");
    }

    /**
     * 获取平台用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return success(platformUsersService.selectPlatformUsersByUserId(userId));
    }

    /**
     * 新增平台用户信息
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:add')")
    @Log(title = "平台用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PlatformUsers platformUsers)
    {
        return toAjax(platformUsersService.insertPlatformUsers(platformUsers));
    }

    /**
     * 修改平台用户信息
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:edit')")
    @Log(title = "平台用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PlatformUsers platformUsers)
    {
        return toAjax(platformUsersService.updatePlatformUsers(platformUsers));
    }

    /**
     * 删除平台用户信息
     */
    @PreAuthorize("@ss.hasPermi('platformusers:platformusers:remove')")
    @Log(title = "平台用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(platformUsersService.deletePlatformUsersByUserIds(userIds));
    }
}
