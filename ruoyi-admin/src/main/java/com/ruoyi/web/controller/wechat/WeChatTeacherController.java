package com.ruoyi.web.controller.wechat;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.platformusers.domain.PlatformUsers;
import com.ruoyi.platformusers.service.IPlatformUsersService;
import com.ruoyi.teacher.domain.DzTeacherList;
import com.ruoyi.teacher.service.IDzTeacherListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师列表Controller
 *
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/wechat/teacher")
public class WeChatTeacherController extends BaseController {
    @Autowired
    private IDzTeacherListService dzTeacherListService;
    @Autowired
    private IPlatformUsersService platformUsersService;
    /**
     * 查询教师列表列表
     */

    @GetMapping("/list")
    public TableDataInfo list(DzTeacherList dzTeacherList) {
//        System.out.println(dzTeacherList);
        startPage();
        List<DzTeacherList> list = dzTeacherListService.selectDzTeacherListList(dzTeacherList);
        for (int i=0; i<list.size(); i++) {
            DzTeacherList dzTeacherList1 = list.get(i);
            //获得教育区域
            String teachingArea = dzTeacherList1.getTeachingArea();
            String teachingArea2 = teachingArea.replace("区", "");
            String[] arr = teachingArea2.split(","); // 使用逗号作为分隔符进行切割
            if (arr.length==1) {
                dzTeacherList1.setTeachingArea1(arr[0]);
            } else  if (arr.length==2) {
                dzTeacherList1.setTeachingArea1(arr[0]);
                dzTeacherList1.setTeachingArea2(arr[1]);

            }else if (arr.length==3) {
                dzTeacherList1.setTeachingArea1(arr[0]);
                dzTeacherList1.setTeachingArea2(arr[1]);
                dzTeacherList1.setTeachingArea3(arr[2]);

            }

            //获得教育科目
            String TeachingParentingSubjects = dzTeacherList1.getTeachingParentingSubjects();
            String[] TeachingParentingSubjectsarr = TeachingParentingSubjects.split(","); // 使用逗号作为分隔符进行切割
            if (TeachingParentingSubjectsarr.length==1) {
                dzTeacherList1.setTeachingParentingSubjects1(TeachingParentingSubjectsarr[0]);
            } else  if (TeachingParentingSubjectsarr.length==2) {
                dzTeacherList1.setTeachingParentingSubjects1(TeachingParentingSubjectsarr[0]);
                dzTeacherList1.setTeachingParentingSubjects2(TeachingParentingSubjectsarr[1]);

            }else if (TeachingParentingSubjectsarr.length==3) {
                dzTeacherList1.setTeachingParentingSubjects1(TeachingParentingSubjectsarr[0]);
                dzTeacherList1.setTeachingParentingSubjects2(TeachingParentingSubjectsarr[1]);
                dzTeacherList1.setTeachingParentingSubjects3(TeachingParentingSubjectsarr[2]);

            }

        }
        System.out.println("zz"+getDataTable(list));
        return getDataTable(list);
    }

    /**
     * 导出教师列表列表
     */

//    @Log(title = "教师列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzTeacherList dzTeacherList) {
        List<DzTeacherList> list = dzTeacherListService.selectDzTeacherListList(dzTeacherList);
        ExcelUtil<DzTeacherList> util = new ExcelUtil<DzTeacherList>(DzTeacherList.class);
        util.exportExcel(response, list, "教师列表数据");
    }

    /**
     * 获取教师列表详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(dzTeacherListService.selectDzTeacherListById(id));
    }

    /**
     * 新增教师列表
     */

//    @Log(title = "教师列表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody DzTeacherList dzTeacherList) {


        DzTeacherList dzTeacherList1 = dzTeacherListService.selectDzTeacherListByteacherResidentId(dzTeacherList.getTeacherResidentId());
        if (dzTeacherList1!=null) {
            return error( "已存在该身份证的用户，请核对用户信息！" );
        }
        int i = dzTeacherListService.insertDzTeacherList(dzTeacherList);


        if (i>0) {
            DzTeacherList dzTeacher = dzTeacherListService.selectDzTeacherListByteacherResidentId(dzTeacherList.getTeacherResidentId());
            Long teacherId = dzTeacher.getId();
            //用户ID
//            Long id = dzTeacherList.getId();
            String userid = dzTeacherList.getUserid();
            //更新用户的教师ID
            PlatformUsers platformUsers=new PlatformUsers();
            //设置用户ID
            platformUsers.setUserId(Long.parseLong(userid));
            //设置老师ID
            platformUsers.setTeacherId(teacherId.toString());

            platformUsersService.updatePlatformUsers(platformUsers);

            return success( teacherId );
        } else {
            return toAjax( i );
        }



    }

    /**
     * 修改教师列表
     */

//    @Log(title = "教师列表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody DzTeacherList dzTeacherList) {




        return toAjax(dzTeacherListService.updateDzTeacherList(dzTeacherList));
    }
    @PostMapping("/updateDzTeacherListbyteacherResidentId")
    public AjaxResult updateDzTeacherListbyteacherResidentId(@RequestBody DzTeacherList dzTeacherList) {


        return toAjax(dzTeacherListService.updateDzTeacherListbyteacherResidentId(dzTeacherList));
    }


    /**
     * 删除教师列表
     */

//    @Log(title = "教师列表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(dzTeacherListService.deleteDzTeacherListByIds(ids));
    }


    /**
     * 上传用户的图片资料
     */

    @PostMapping("/updataimgsfile")
    public AjaxResult updatafile(DzTeacherList dzTeacherList, @RequestParam("imgsfile") MultipartFile file) throws Exception {
        System.out.println("zzz" + dzTeacherList.getType());
        if (!file.isEmpty()) {

            String Remark = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (dzTeacherList.getType().equals("2")) {
                //身份证正面
                dzTeacherList.setIdcardFrontPhoto(Remark);
            } else if (dzTeacherList.getType().equals("3")) {
                //身份证反面
                dzTeacherList.setIdcardBackPhoto(Remark);
            } else if (dzTeacherList.getType().equals("4")) {
                //证书
                dzTeacherList.setCertificatePhoto(Remark) ;
            }


            return  toAjax(dzTeacherListService.updateDzTeacherList(dzTeacherList));

        }else {
            return error("上传图片异常，请联系管理员");
        }

    }
}
