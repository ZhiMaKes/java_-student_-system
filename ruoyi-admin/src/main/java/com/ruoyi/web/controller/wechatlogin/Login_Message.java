package com.ruoyi.web.controller.wechatlogin;

public class Login_Message {

    String phone_encryptedData;
    String phone_iv;

    public String getPhone_encryptedData() {
        return phone_encryptedData;
    }

    public void setPhone_encryptedData(String phone_encryptedData) {
        this.phone_encryptedData = phone_encryptedData;
    }

    public String getPhone_iv() {
        return phone_iv;
    }

    public void setPhone_iv(String phone_iv) {
        this.phone_iv = phone_iv;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    String code;
}
