package com.ruoyi.web.controller.wechat;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.TimeUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.student.domain.DzStudentList;
import com.ruoyi.student.service.IDzStudentListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学员列表Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/wechat/student")
public class WeChatStudentController extends BaseController
{
    @Autowired
    private IDzStudentListService dzStudentListService;

    /**
     * 查询学员列表列表
     */

    @GetMapping("/list")
    public TableDataInfo list( DzStudentList dzStudentList)

    {
        startPage();
        List<DzStudentList> list = dzStudentListService.selectDzStudentListList(dzStudentList);
        return getDataTable2(list);
    }

    /**
     * 导出学员列表列表
     */

//    @Log(title = "学员列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzStudentList dzStudentList)
    {
        List<DzStudentList> list = dzStudentListService.selectDzStudentListList(dzStudentList);
        ExcelUtil<DzStudentList> util = new ExcelUtil<DzStudentList>(DzStudentList.class);
        util.exportExcel(response, list, "学员列表数据");
    }

    /**
     * 获取学员列表详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dzStudentListService.selectDzStudentListById(id));
    }

    /**
     * 新增学员列表
     */

//    @Log(title = "学员列表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody DzStudentList dzStudentList)

    {
        //2完成 1已经完成
        dzStudentList.setState("2");
        dzStudentList.setRemark1(""+ TimeUtils.gettime());
        return toAjax(dzStudentListService.insertDzStudentList(dzStudentList));
    }

    /**
     * 修改学员列表
     */

//    @Log(title = "学员列表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody DzStudentList dzStudentList)
    {
        return toAjax(dzStudentListService.updateDzStudentList(dzStudentList));
    }

    /**
     * 删除学员列表
     */

//    @Log(title = "学员列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dzStudentListService.deleteDzStudentListByIds(ids));
    }
    @GetMapping("/deteleid/{id}")
    public AjaxResult deteleid(@PathVariable("id") Long id)
    {
        return toAjax(dzStudentListService.deleteDzStudentListById(id));
    }
}
