package com.ruoyi.web.controller.student;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.TimeUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.student.domain.DzStudentList;
import com.ruoyi.student.service.IDzStudentListService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学员列表Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/student/studentlist")
public class DzStudentListController extends BaseController
{
    @Autowired
    private IDzStudentListService dzStudentListService;

    /**
     * 查询学员列表列表
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:list')")
    @GetMapping("/list")
    public TableDataInfo list(DzStudentList dzStudentList)
    {
        startPage();
        List<DzStudentList> list = dzStudentListService.selectDzStudentListList(dzStudentList);
        return getDataTable(list);
    }

    /**
     * 导出学员列表列表
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:export')")
    @Log(title = "学员列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzStudentList dzStudentList)
    {
        List<DzStudentList> list = dzStudentListService.selectDzStudentListList(dzStudentList);
        ExcelUtil<DzStudentList> util = new ExcelUtil<DzStudentList>(DzStudentList.class);
        util.exportExcel(response, list, "学员列表数据");
    }

    /**
     * 获取学员列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dzStudentListService.selectDzStudentListById(id));
    }

    /**
     * 新增学员列表
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:add')")
    @Log(title = "学员列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzStudentList dzStudentList)
    {  dzStudentList.setRemark1(""+ TimeUtils.gettime());
        return toAjax(dzStudentListService.insertDzStudentList(dzStudentList));
    }

    /**
     * 修改学员列表
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:edit')")
    @Log(title = "学员列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzStudentList dzStudentList)
    {
        System.out.println("开始");
        System.out.println(dzStudentList);
        System.out.println("结束");
        return toAjax(dzStudentListService.updateDzStudentList(dzStudentList));
    }

    /**
     * 删除学员列表
     */
    @PreAuthorize("@ss.hasPermi('student:studentlist:remove')")
    @Log(title = "学员列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dzStudentListService.deleteDzStudentListByIds(ids));
    }
}
