package com.ruoyi.article.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 文章管理对象 dz_article
 * 
 * @author ruoyi
 * @date 2024-01-06
 */
public class Article extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 文章所属分类 */
    @Excel(name = "文章所属分类")
    private String type;

    /** 文章名称 */
    @Excel(name = "文章名称")
    private String title;

    /** 文章封面图片 */
    @Excel(name = "文章封面图片")
    private String img;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    /** 文章作者 */
    @Excel(name = "文章作者")
    private String author;

    /** 文章简介 */
    @Excel(name = "文章简介")
    private String introduction;

    /** 文章内容 */
    @Excel(name = "文章内容")
    private String content;

    /** 是否热门 */
    @Excel(name = "是否热门")
    private String popular;

    /** 是否轮播图 */
    @Excel(name = "是否轮播图")
    private String banner;

    /** 图片列表 */
    @Excel(name = "图片列表")
    private String imgList;

    /** 观看数量 */
    @Excel(name = "观看数量")
    private Long seeNum;

    /** 喜欢数量 */
    @Excel(name = "喜欢数量")
    private Long likesNum;

    /** 分享数量 */
    @Excel(name = "分享数量")
    private Long commentNum;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String createdAt;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark3;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark4;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark5;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark6;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark7;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark8;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark9;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark10;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setIntroduction(String introduction) 
    {
        this.introduction = introduction;
    }

    public String getIntroduction() 
    {
        return introduction;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPopular(String popular) 
    {
        this.popular = popular;
    }

    public String getPopular() 
    {
        return popular;
    }
    public void setBanner(String banner) 
    {
        this.banner = banner;
    }

    public String getBanner() 
    {
        return banner;
    }
    public void setImgList(String imgList) 
    {
        this.imgList = imgList;
    }

    public String getImgList() 
    {
        return imgList;
    }
    public void setSeeNum(Long seeNum) 
    {
        this.seeNum = seeNum;
    }

    public Long getSeeNum() 
    {
        return seeNum;
    }
    public void setLikesNum(Long likesNum) 
    {
        this.likesNum = likesNum;
    }

    public Long getLikesNum() 
    {
        return likesNum;
    }
    public void setCommentNum(Long commentNum) 
    {
        this.commentNum = commentNum;
    }

    public Long getCommentNum() 
    {
        return commentNum;
    }
    public void setRemark1(String remark1) 
    {
        this.remark1 = remark1;
    }

    public String getRemark1() 
    {
        return remark1;
    }
    public void setCreatedAt(String createdAt) 
    {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() 
    {
        return createdAt;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("title", getTitle())
            .append("img", getImg())
            .append("state", getState())
            .append("author", getAuthor())
            .append("introduction", getIntroduction())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("popular", getPopular())
            .append("banner", getBanner())
            .append("imgList", getImgList())
            .append("seeNum", getSeeNum())
            .append("likesNum", getLikesNum())
            .append("commentNum", getCommentNum())
            .append("remark1", getRemark1())
            .append("createdAt", getCreatedAt())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
