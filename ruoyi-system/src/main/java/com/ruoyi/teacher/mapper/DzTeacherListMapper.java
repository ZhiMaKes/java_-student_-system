package com.ruoyi.teacher.mapper;

import java.util.List;
import com.ruoyi.teacher.domain.DzTeacherList;

/**
 * 教师列表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface DzTeacherListMapper 
{
    /**
     * 查询教师列表
     * 
     * @param id 教师列表主键
     * @return 教师列表
     */
    public DzTeacherList selectDzTeacherListById(Long id);
    public DzTeacherList selectDzTeacherListByteacherResidentId(String teacherResidentId);



    /**
     * 查询教师列表列表
     * 
     * @param dzTeacherList 教师列表
     * @return 教师列表集合
     */
    public List<DzTeacherList> selectDzTeacherListList(DzTeacherList dzTeacherList);

    /**
     * 新增教师列表
     * 
     * @param dzTeacherList 教师列表
     * @return 结果
     */
    public int insertDzTeacherList(DzTeacherList dzTeacherList);

    /**
     * 修改教师列表
     * 
     * @param dzTeacherList 教师列表
     * @return 结果
     */
    public int updateDzTeacherList(DzTeacherList dzTeacherList);
    public int updateDzTeacherListbyteacherResidentId(DzTeacherList dzTeacherList);


    /**
     * 删除教师列表
     * 
     * @param id 教师列表主键
     * @return 结果
     */
    public int deleteDzTeacherListById(Long id);

    /**
     * 批量删除教师列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzTeacherListByIds(Long[] ids);
}
