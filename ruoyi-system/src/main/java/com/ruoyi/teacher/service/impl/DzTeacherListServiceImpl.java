package com.ruoyi.teacher.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.teacher.mapper.DzTeacherListMapper;
import com.ruoyi.teacher.domain.DzTeacherList;
import com.ruoyi.teacher.service.IDzTeacherListService;

/**
 * 教师列表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class DzTeacherListServiceImpl implements IDzTeacherListService 
{
    @Autowired
    private DzTeacherListMapper dzTeacherListMapper;

    /**
     * 查询教师列表
     * 
     * @param id 教师列表主键
     * @return 教师列表
     */
    @Override
    public DzTeacherList selectDzTeacherListById(Long id)
    {
        return dzTeacherListMapper.selectDzTeacherListById(id);
    }

    /**
     * 查询教师列表列表
     * 
     * @param dzTeacherList 教师列表
     * @return 教师列表
     */
    @Override
    public List<DzTeacherList> selectDzTeacherListList(DzTeacherList dzTeacherList)
    {
        return dzTeacherListMapper.selectDzTeacherListList(dzTeacherList);
    }
    public  DzTeacherList  selectDzTeacherListByteacherResidentId(String teacherResidentId)
    {
        return dzTeacherListMapper.selectDzTeacherListByteacherResidentId(teacherResidentId);
    }



    /**
     * 新增教师列表
     * 
     * @param dzTeacherList 教师列表
     * @return 结果
     */
    @Override
    public int insertDzTeacherList(DzTeacherList dzTeacherList)
    {
        return dzTeacherListMapper.insertDzTeacherList(dzTeacherList);
    }

    /**
     * 修改教师列表
     * 
     * @param dzTeacherList 教师列表
     * @return 结果
     */
    @Override
    public int updateDzTeacherList(DzTeacherList dzTeacherList)
    {
        return dzTeacherListMapper.updateDzTeacherList(dzTeacherList);
    }
    @Override
    public int updateDzTeacherListbyteacherResidentId(DzTeacherList dzTeacherList)
    {
        return dzTeacherListMapper.updateDzTeacherListbyteacherResidentId(dzTeacherList);
    }


    /**
     * 批量删除教师列表
     * 
     * @param ids 需要删除的教师列表主键
     * @return 结果
     */
    @Override
    public int deleteDzTeacherListByIds(Long[] ids)
    {
        return dzTeacherListMapper.deleteDzTeacherListByIds(ids);
    }

    /**
     * 删除教师列表信息
     * 
     * @param id 教师列表主键
     * @return 结果
     */
    @Override
    public int deleteDzTeacherListById(Long id)
    {
        return dzTeacherListMapper.deleteDzTeacherListById(id);
    }
}
