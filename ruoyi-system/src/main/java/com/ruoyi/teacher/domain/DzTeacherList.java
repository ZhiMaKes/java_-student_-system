package com.ruoyi.teacher.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 教师对象 dz_teacher_list
 *
 * @author ruoyi
 * @date 2024-01-05
 */
public class DzTeacherList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 教师姓名 */
    @Excel(name = "教师姓名")
    private String teacherName;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    private String userid;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String teacherPhone;

    /** 教师身份证号 */
    @Excel(name = "教师身份证号")
    private String teacherResidentId;

    /** 教师性别（0男 1女 2未知） */
    @Excel(name = "教师性别", readConverterExp = "0=男,1=女,2=未知")
    private String teacherGender;

    /** 教师年龄 */
    @Excel(name = "教师年龄")
    private String teacherAge;

    /** 教师学校 */
    @Excel(name = "教师学校")
    private String teacherSchool;

    /** 教师专业 */
    @Excel(name = "教师专业")
    private String teacherSpeciality;

    /** 教师身份 */
    @Excel(name = "教师身份")
    private String teacherIdentity;

    /** 教师教龄 */
    @Excel(name = "教师教龄")
    private String teachingLength;

    /** 授课方式 */
    @Excel(name = "授课方式")
    private String teachingMethod;

    /** 家教时间 */
    @Excel(name = "家教时间")
    private String teachingTime;

    /** 家教费用 */
    @Excel(name = "家教费用")
    private String teachingPrice;

    /** 家教区域 */
    @Excel(name = "家教区域")
    private String teachingArea;

    public String getTeachingArea2() {
        return teachingArea2;
    }

    public void setTeachingArea2(String teachingArea2) {
        this.teachingArea2 = teachingArea2;
    }

    public String getTeachingArea3() {
        return teachingArea3;
    }

    public void setTeachingArea3(String teachingArea3) {
        this.teachingArea3 = teachingArea3;
    }

    public String getTeachingArea1() {
        return teachingArea1;
    }

    public void setTeachingArea1(String teachingArea1) {
        this.teachingArea1 = teachingArea1;
    }

    private String teachingArea1;
    private String teachingArea2;
    private String teachingArea3;

    /** 居住地址 */
    @Excel(name = "居住地址")
    private String teacherAddress;

    /** 家教科目 */
    @Excel(name = "家教科目")
    private String teachingParentingSubjects;

    private String teachingParentingSubjects1;

    public String getTeachingParentingSubjects1() {
        return teachingParentingSubjects1;
    }

    public void setTeachingParentingSubjects1(String teachingParentingSubjects1) {
        this.teachingParentingSubjects1 = teachingParentingSubjects1;
    }

    public String getTeachingParentingSubjects2() {
        return teachingParentingSubjects2;
    }

    public void setTeachingParentingSubjects2(String teachingParentingSubjects2) {
        this.teachingParentingSubjects2 = teachingParentingSubjects2;
    }

    public String getTeachingParentingSubjects3() {
        return teachingParentingSubjects3;
    }

    public void setTeachingParentingSubjects3(String teachingParentingSubjects3) {
        this.teachingParentingSubjects3 = teachingParentingSubjects3;
    }

    private String teachingParentingSubjects2;
    private String teachingParentingSubjects3;

    /** 自我描述 */
    @Excel(name = "自我描述")
    private String teacherDescribe;

    /** 家教经验 */
    @Excel(name = "家教经验")
    private String teacherExperience;

    /** 所获证书 */
    @Excel(name = "所获证书")
    private String teacherCertificate;

    /** 教员要求 */
    @Excel(name = "教员要求")
    private String teacherRequirements;

    /** 最近登录时间 */
    @Excel(name = "最近登录时间")
    private String recentLoginTime;

    /** 简历照片 */
    @Excel(name = "简历照片")
    private String resumePhoto;

    /** 身份证正面照片 */
    @Excel(name = "身份证正面照片")
    private String idcardFrontPhoto;

    /** 身份证反面照片 */
    @Excel(name = "身份证反面照片")
    private String idcardBackPhoto;

    /** 审核状态（0未审核 1已审核） */
    @Excel(name = "审核状态", readConverterExp = "0=未审核,1=已审核")
    private String auditStatus;

    /** 证书照片 */
    @Excel(name = "证书照片")
    private String certificatePhoto;

    /** 完成状态 */
    @Excel(name = "完成状态")
    private String state;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark3;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark4;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark5;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark6;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark7;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark8;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark9;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark10;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }

    public String getTeacherName()
    {
        return teacherName;
    }
    public void setTeacherPhone(String teacherPhone)
    {
        this.teacherPhone = teacherPhone;
    }

    public String getTeacherPhone()
    {
        return teacherPhone;
    }
    public void setTeacherResidentId(String teacherResidentId)
    {
        this.teacherResidentId = teacherResidentId;
    }

    public String getTeacherResidentId()
    {
        return teacherResidentId;
    }
    public void setTeacherGender(String teacherGender)
    {
        this.teacherGender = teacherGender;
    }

    public String getTeacherGender()
    {
        return teacherGender;
    }
    public void setTeacherAge(String teacherAge)
    {
        this.teacherAge = teacherAge;
    }

    public String getTeacherAge()
    {
        return teacherAge;
    }
    public void setTeacherSchool(String teacherSchool)
    {
        this.teacherSchool = teacherSchool;
    }

    public String getTeacherSchool()
    {
        return teacherSchool;
    }
    public void setTeacherSpeciality(String teacherSpeciality)
    {
        this.teacherSpeciality = teacherSpeciality;
    }

    public String getTeacherSpeciality()
    {
        return teacherSpeciality;
    }
    public void setTeacherIdentity(String teacherIdentity)
    {
        this.teacherIdentity = teacherIdentity;
    }

    public String getTeacherIdentity()
    {
        return teacherIdentity;
    }
    public void setTeachingLength(String teachingLength)
    {
        this.teachingLength = teachingLength;
    }

    public String getTeachingLength()
    {
        return teachingLength;
    }
    public void setTeachingMethod(String teachingMethod)
    {
        this.teachingMethod = teachingMethod;
    }

    public String getTeachingMethod()
    {
        return teachingMethod;
    }
    public void setTeachingTime(String teachingTime)
    {
        this.teachingTime = teachingTime;
    }

    public String getTeachingTime()
    {
        return teachingTime;
    }
    public void setTeachingPrice(String teachingPrice)
    {
        this.teachingPrice = teachingPrice;
    }

    public String getTeachingPrice()
    {
        return teachingPrice;
    }
    public void setTeachingArea(String teachingArea)
    {
        this.teachingArea = teachingArea;
    }

    public String getTeachingArea()
    {
        return teachingArea;
    }
    public void setTeacherAddress(String teacherAddress)
    {
        this.teacherAddress = teacherAddress;
    }

    public String getTeacherAddress()
    {
        return teacherAddress;
    }
    public void setTeachingParentingSubjects(String teachingParentingSubjects)
    {
        this.teachingParentingSubjects = teachingParentingSubjects;
    }

    public String getTeachingParentingSubjects()
    {
        return teachingParentingSubjects;
    }
    public void setTeacherDescribe(String teacherDescribe)
    {
        this.teacherDescribe = teacherDescribe;
    }

    public String getTeacherDescribe()
    {
        return teacherDescribe;
    }
    public void setTeacherExperience(String teacherExperience)
    {
        this.teacherExperience = teacherExperience;
    }

    public String getTeacherExperience()
    {
        return teacherExperience;
    }
    public void setTeacherCertificate(String teacherCertificate)
    {
        this.teacherCertificate = teacherCertificate;
    }

    public String getTeacherCertificate()
    {
        return teacherCertificate;
    }
    public void setTeacherRequirements(String teacherRequirements)
    {
        this.teacherRequirements = teacherRequirements;
    }

    public String getTeacherRequirements()
    {
        return teacherRequirements;
    }
    public void setRecentLoginTime(String recentLoginTime)
    {
        this.recentLoginTime = recentLoginTime;
    }

    public String getRecentLoginTime()
    {
        return recentLoginTime;
    }
    public void setResumePhoto(String resumePhoto)
    {
        this.resumePhoto = resumePhoto;
    }

    public String getResumePhoto()
    {
        return resumePhoto;
    }
    public void setIdcardFrontPhoto(String idcardFrontPhoto)
    {
        this.idcardFrontPhoto = idcardFrontPhoto;
    }

    public String getIdcardFrontPhoto()
    {
        return idcardFrontPhoto;
    }
    public void setIdcardBackPhoto(String idcardBackPhoto)
    {
        this.idcardBackPhoto = idcardBackPhoto;
    }

    public String getIdcardBackPhoto()
    {
        return idcardBackPhoto;
    }
    public void setAuditStatus(String auditStatus)
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus()
    {
        return auditStatus;
    }
    public void setCertificatePhoto(String certificatePhoto)
    {
        this.certificatePhoto = certificatePhoto;
    }

    public String getCertificatePhoto()
    {
        return certificatePhoto;
    }
    public void setState(String state)
    {
        this.state = state;
    }

    public String getState()
    {
        return state;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setRemark1(String remark1)
    {
        this.remark1 = remark1;
    }

    public String getRemark1()
    {
        return remark1;
    }
    public void setRemark2(String remark2)
    {
        this.remark2 = remark2;
    }

    public String getRemark2()
    {
        return remark2;
    }
    public void setRemark3(String remark3)
    {
        this.remark3 = remark3;
    }

    public String getRemark3()
    {
        return remark3;
    }
    public void setRemark4(String remark4)
    {
        this.remark4 = remark4;
    }

    public String getRemark4()
    {
        return remark4;
    }
    public void setRemark5(String remark5)
    {
        this.remark5 = remark5;
    }

    public String getRemark5()
    {
        return remark5;
    }
    public void setRemark6(String remark6)
    {
        this.remark6 = remark6;
    }

    public String getRemark6()
    {
        return remark6;
    }
    public void setRemark7(String remark7)
    {
        this.remark7 = remark7;
    }

    public String getRemark7()
    {
        return remark7;
    }
    public void setRemark8(String remark8)
    {
        this.remark8 = remark8;
    }

    public String getRemark8()
    {
        return remark8;
    }
    public void setRemark9(String remark9)
    {
        this.remark9 = remark9;
    }

    public String getRemark9()
    {
        return remark9;
    }
    public void setRemark10(String remark10)
    {
        this.remark10 = remark10;
    }

    public String getRemark10()
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("teacherName", getTeacherName())
                .append("teacherPhone", getTeacherPhone())
                .append("teacherResidentId", getTeacherResidentId())
                .append("teacherGender", getTeacherGender())
                .append("teacherAge", getTeacherAge())
                .append("teacherSchool", getTeacherSchool())
                .append("teacherSpeciality", getTeacherSpeciality())
                .append("teacherIdentity", getTeacherIdentity())
                .append("teachingLength", getTeachingLength())
                .append("teachingMethod", getTeachingMethod())
                .append("teachingTime", getTeachingTime())
                .append("teachingPrice", getTeachingPrice())
                .append("teachingArea", getTeachingArea())
                .append("teacherAddress", getTeacherAddress())
                .append("teachingParentingSubjects", getTeachingParentingSubjects())
                .append("teacherDescribe", getTeacherDescribe())
                .append("teacherExperience", getTeacherExperience())
                .append("teacherCertificate", getTeacherCertificate())
                .append("teacherRequirements", getTeacherRequirements())
                .append("recentLoginTime", getRecentLoginTime())
                .append("resumePhoto", getResumePhoto())
                .append("idcardFrontPhoto", getIdcardFrontPhoto())
                .append("idcardBackPhoto", getIdcardBackPhoto())
                .append("auditStatus", getAuditStatus())
                .append("certificatePhoto", getCertificatePhoto())
                .append("state", getState())
                .append("delFlag", getDelFlag())
                .append("remark1", getRemark1())
                .append("remark2", getRemark2())
                .append("remark3", getRemark3())
                .append("remark4", getRemark4())
                .append("remark5", getRemark5())
                .append("remark6", getRemark6())
                .append("remark7", getRemark7())
                .append("remark8", getRemark8())
                .append("remark9", getRemark9())
                .append("remark10", getRemark10())
                .toString();
    }
}
