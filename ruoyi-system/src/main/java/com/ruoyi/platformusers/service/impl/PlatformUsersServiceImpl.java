package com.ruoyi.platformusers.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platformusers.mapper.PlatformUsersMapper;
import com.ruoyi.platformusers.domain.PlatformUsers;
import com.ruoyi.platformusers.service.IPlatformUsersService;

/**
 * 平台用户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-05
 */
@Service
public class PlatformUsersServiceImpl implements IPlatformUsersService 
{
    @Autowired
    private PlatformUsersMapper platformUsersMapper;

    /**
     * 查询平台用户信息
     * 
     * @param userId 平台用户信息主键
     * @return 平台用户信息
     */
    @Override
    public PlatformUsers selectPlatformUsersByUserId(Long userId)
    {
        return platformUsersMapper.selectPlatformUsersByUserId(userId);
    }

    /**
     * 查询平台用户信息列表
     * 
     * @param platformUsers 平台用户信息
     * @return 平台用户信息
     */
    @Override
    public List<PlatformUsers> selectPlatformUsersList(PlatformUsers platformUsers)
    {
        return platformUsersMapper.selectPlatformUsersList(platformUsers);
    }

    /**
     * 新增平台用户信息
     * 
     * @param platformUsers 平台用户信息
     * @return 结果
     */
    @Override
    public int insertPlatformUsers(PlatformUsers platformUsers)
    {
        platformUsers.setCreateTime(DateUtils.getNowDate());
        return platformUsersMapper.insertPlatformUsers(platformUsers);
    }

    /**
     * 修改平台用户信息
     * 
     * @param platformUsers 平台用户信息
     * @return 结果
     */
    @Override
    public int updatePlatformUsers(PlatformUsers platformUsers)
    {
        platformUsers.setUpdateTime(DateUtils.getNowDate());
        return platformUsersMapper.updatePlatformUsers(platformUsers);
    }

    /**
     * 批量删除平台用户信息
     * 
     * @param userIds 需要删除的平台用户信息主键
     * @return 结果
     */
    @Override
    public int deletePlatformUsersByUserIds(Long[] userIds)
    {
        return platformUsersMapper.deletePlatformUsersByUserIds(userIds);
    }

    /**
     * 删除平台用户信息信息
     * 
     * @param userId 平台用户信息主键
     * @return 结果
     */
    @Override
    public int deletePlatformUsersByUserId(Long userId)
    {
        return platformUsersMapper.deletePlatformUsersByUserId(userId);
    }
}
