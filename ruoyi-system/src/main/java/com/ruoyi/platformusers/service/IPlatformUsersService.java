package com.ruoyi.platformusers.service;

import java.util.List;
import com.ruoyi.platformusers.domain.PlatformUsers;

/**
 * 平台用户信息Service接口
 * 
 * @author ruoyi
 * @date 2024-01-05
 */
public interface IPlatformUsersService 
{
    /**
     * 查询平台用户信息
     * 
     * @param userId 平台用户信息主键
     * @return 平台用户信息
     */
    public PlatformUsers selectPlatformUsersByUserId(Long userId);

    /**
     * 查询平台用户信息列表
     * 
     * @param platformUsers 平台用户信息
     * @return 平台用户信息集合
     */
    public List<PlatformUsers> selectPlatformUsersList(PlatformUsers platformUsers);

    /**
     * 新增平台用户信息
     * 
     * @param platformUsers 平台用户信息
     * @return 结果
     */
    public int insertPlatformUsers(PlatformUsers platformUsers);

    /**
     * 修改平台用户信息
     * 
     * @param platformUsers 平台用户信息
     * @return 结果
     */
    public int updatePlatformUsers(PlatformUsers platformUsers);

    /**
     * 批量删除平台用户信息
     * 
     * @param userIds 需要删除的平台用户信息主键集合
     * @return 结果
     */
    public int deletePlatformUsersByUserIds(Long[] userIds);

    /**
     * 删除平台用户信息信息
     * 
     * @param userId 平台用户信息主键
     * @return 结果
     */
    public int deletePlatformUsersByUserId(Long userId);
}
