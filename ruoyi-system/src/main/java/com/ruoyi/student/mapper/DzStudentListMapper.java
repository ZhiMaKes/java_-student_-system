package com.ruoyi.student.mapper;

import java.util.List;
import com.ruoyi.student.domain.DzStudentList;

/**
 * 学员列表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface DzStudentListMapper 
{
    /**
     * 查询学员列表
     * 
     * @param id 学员列表主键
     * @return 学员列表
     */
    public DzStudentList selectDzStudentListById(Long id);

    /**
     * 查询学员列表列表
     * 
     * @param dzStudentList 学员列表
     * @return 学员列表集合
     */
    public List<DzStudentList> selectDzStudentListList(DzStudentList dzStudentList);

    /**
     * 新增学员列表
     * 
     * @param dzStudentList 学员列表
     * @return 结果
     */
    public int insertDzStudentList(DzStudentList dzStudentList);

    /**
     * 修改学员列表
     * 
     * @param dzStudentList 学员列表
     * @return 结果
     */
    public int updateDzStudentList(DzStudentList dzStudentList);

    /**
     * 删除学员列表
     * 
     * @param id 学员列表主键
     * @return 结果
     */
    public int deleteDzStudentListById(Long id);

    /**
     * 批量删除学员列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzStudentListByIds(Long[] ids);
}
