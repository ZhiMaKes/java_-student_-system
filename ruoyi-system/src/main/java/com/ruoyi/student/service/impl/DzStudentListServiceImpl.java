package com.ruoyi.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.student.mapper.DzStudentListMapper;
import com.ruoyi.student.domain.DzStudentList;
import com.ruoyi.student.service.IDzStudentListService;

/**
 * 学员列表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class DzStudentListServiceImpl implements IDzStudentListService 
{
    @Autowired
    private DzStudentListMapper dzStudentListMapper;

    /**
     * 查询学员列表
     * 
     * @param id 学员列表主键
     * @return 学员列表
     */
    @Override
    public DzStudentList selectDzStudentListById(Long id)
    {
        return dzStudentListMapper.selectDzStudentListById(id);
    }

    /**
     * 查询学员列表列表
     * 
     * @param dzStudentList 学员列表
     * @return 学员列表
     */
    @Override
    public List<DzStudentList> selectDzStudentListList(DzStudentList dzStudentList)
    {
        return dzStudentListMapper.selectDzStudentListList(dzStudentList);
    }

    /**
     * 新增学员列表
     * 
     * @param dzStudentList 学员列表
     * @return 结果
     */
    @Override
    public int insertDzStudentList(DzStudentList dzStudentList)
    {
        return dzStudentListMapper.insertDzStudentList(dzStudentList);
    }

    /**
     * 修改学员列表
     * 
     * @param dzStudentList 学员列表
     * @return 结果
     */
    @Override
    public int updateDzStudentList(DzStudentList dzStudentList)
    {
        return dzStudentListMapper.updateDzStudentList(dzStudentList);
    }

    /**
     * 批量删除学员列表
     * 
     * @param ids 需要删除的学员列表主键
     * @return 结果
     */
    @Override
    public int deleteDzStudentListByIds(Long[] ids)
    {
        return dzStudentListMapper.deleteDzStudentListByIds(ids);
    }


    /**
     * 删除学员列表信息
     * 
     * @param id 学员列表主键
     * @return 结果
     */
    @Override
    public int deleteDzStudentListById(Long id)
    {
        return dzStudentListMapper.deleteDzStudentListById(id);
    }
}
