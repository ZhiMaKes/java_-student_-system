package com.ruoyi.student.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学员列表对象 dz_student_list
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public class DzStudentList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 序号 */
    private Long id;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    private String auditStatus;


    /** 联系方式 */
    @Excel(name = "联系方式")
    private String studentPhone;

    /** 学生性别（0男 1女 2未知） */
    @Excel(name = "学生性别", readConverterExp = "0=男,1=女,2=未知")
    private String studentGender;

    /** 学生年级 */
    @Excel(name = "学生年级")
    private String studentGrade;

    /** 家教科目 */
    @Excel(name = "家教科目")
    private String studentParentingSubjects;

    /** 家教区域 */
    @Excel(name = "家教区域")
    private String studentArea;

    /** 授课方式 */
    @Excel(name = "授课方式")
    private String studentTeachingMethod;

    /** 家教费用 */
    @Excel(name = "家教费用")
    private String price;

    /** 参与数量 */
    @Excel(name = "参与数量")
    private String number;

    /** 家教时间 */
    @Excel(name = "家教时间")
    private String studentTime;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String studentAddress;

    /** 学员描述 */
    @Excel(name = "学员描述")
    private String studentDescribe;

    /** 教员要求 */
    @Excel(name = "教员要求")
    private String teacherRequirements;

    /** 完成状态 */
    @Excel(name = "完成状态")
    private String state;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark1;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark3;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark4;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark5;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark6;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark7;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark8;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark9;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remark10;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setStudentPhone(String studentPhone) 
    {
        this.studentPhone = studentPhone;
    }

    public String getStudentPhone() 
    {
        return studentPhone;
    }
    public void setStudentGender(String studentGender) 
    {
        this.studentGender = studentGender;
    }

    public String getStudentGender() 
    {
        return studentGender;
    }
    public void setStudentGrade(String studentGrade) 
    {
        this.studentGrade = studentGrade;
    }

    public String getStudentGrade() 
    {
        return studentGrade;
    }
    public void setStudentParentingSubjects(String studentParentingSubjects) 
    {
        this.studentParentingSubjects = studentParentingSubjects;
    }

    public String getStudentParentingSubjects() 
    {
        return studentParentingSubjects;
    }
    public void setStudentArea(String studentArea) 
    {
        this.studentArea = studentArea;
    }

    public String getStudentArea() 
    {
        return studentArea;
    }
    public void setStudentTeachingMethod(String studentTeachingMethod) 
    {
        this.studentTeachingMethod = studentTeachingMethod;
    }

    public String getStudentTeachingMethod() 
    {
        return studentTeachingMethod;
    }
    public void setPrice(String price) 
    {
        this.price = price;
    }

    public String getPrice() 
    {
        return price;
    }
    public void setNumber(String number) 
    {
        this.number = number;
    }

    public String getNumber() 
    {
        return number;
    }
    public void setStudentTime(String studentTime) 
    {
        this.studentTime = studentTime;
    }

    public String getStudentTime() 
    {
        return studentTime;
    }
    public void setStudentAddress(String studentAddress) 
    {
        this.studentAddress = studentAddress;
    }

    public String getStudentAddress() 
    {
        return studentAddress;
    }
    public void setStudentDescribe(String studentDescribe) 
    {
        this.studentDescribe = studentDescribe;
    }

    public String getStudentDescribe() 
    {
        return studentDescribe;
    }
    public void setTeacherRequirements(String teacherRequirements) 
    {
        this.teacherRequirements = teacherRequirements;
    }

    public String getTeacherRequirements() 
    {
        return teacherRequirements;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setRemark1(String remark1) 
    {
        this.remark1 = remark1;
    }

    public String getRemark1() 
    {
        return remark1;
    }
    public void setRemark2(String remark2) 
    {
        this.remark2 = remark2;
    }

    public String getRemark2() 
    {
        return remark2;
    }
    public void setRemark3(String remark3) 
    {
        this.remark3 = remark3;
    }

    public String getRemark3() 
    {
        return remark3;
    }
    public void setRemark4(String remark4) 
    {
        this.remark4 = remark4;
    }

    public String getRemark4() 
    {
        return remark4;
    }
    public void setRemark5(String remark5) 
    {
        this.remark5 = remark5;
    }

    public String getRemark5() 
    {
        return remark5;
    }
    public void setRemark6(String remark6) 
    {
        this.remark6 = remark6;
    }

    public String getRemark6() 
    {
        return remark6;
    }
    public void setRemark7(String remark7) 
    {
        this.remark7 = remark7;
    }

    public String getRemark7() 
    {
        return remark7;
    }
    public void setRemark8(String remark8) 
    {
        this.remark8 = remark8;
    }

    public String getRemark8() 
    {
        return remark8;
    }
    public void setRemark9(String remark9) 
    {
        this.remark9 = remark9;
    }

    public String getRemark9() 
    {
        return remark9;
    }
    public void setRemark10(String remark10) 
    {
        this.remark10 = remark10;
    }

    public String getRemark10() 
    {
        return remark10;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentName", getStudentName())
            .append("studentPhone", getStudentPhone())
            .append("studentGender", getStudentGender())
            .append("studentGrade", getStudentGrade())
            .append("studentParentingSubjects", getStudentParentingSubjects())
            .append("studentArea", getStudentArea())
            .append("studentTeachingMethod", getStudentTeachingMethod())
            .append("price", getPrice())
            .append("number", getNumber())
            .append("studentTime", getStudentTime())
            .append("studentAddress", getStudentAddress())
            .append("studentDescribe", getStudentDescribe())
            .append("teacherRequirements", getTeacherRequirements())
            .append("state", getState())
            .append("delFlag", getDelFlag())
            .append("remark1", getRemark1())
            .append("remark2", getRemark2())
            .append("remark3", getRemark3())
            .append("remark4", getRemark4())
            .append("remark5", getRemark5())
            .append("remark6", getRemark6())
            .append("remark7", getRemark7())
            .append("remark8", getRemark8())
            .append("remark9", getRemark9())
            .append("remark10", getRemark10())
            .toString();
    }
}
