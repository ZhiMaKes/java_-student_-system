package com.ruoyi.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

    public static String gettime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = new Date();
        String currentTime = dateFormat.format(date1);
//        System.out.println(currentTime);
                return currentTime ;
    }
}
